#include <iostream>    2


using namespace std;

class BankAccount{
    
    char depositorName[30];
    char accountType[10];
    int accountNumber = 0;
    float accountBalance;
    
public:
    void registerAccount(int counter);
    void accDeposit(int counter);
    void accWithdraw(int counter);
    void accDetails(int counter);
};
void BankAccount::registerAccount(int counter){
    
    cout << "Enter Customer Details ";
    cout << counter;
    cout << "\n";
    cout << "Name of Customer: ";
    cin >> BankAccount::depositorName;
    cout << "\n";
    cout << "Type of Account: ";
    cin >> BankAccount::accountType;
    cout << "\n";
    cout << "Account Number: ";
    cin >> BankAccount::accountNumber;
    cout << "\n";
    cout << "Balance: ";
    cin >> BankAccount::accountBalance;
    cout << "\n" << endl;
}

void BankAccount::accDeposit(int counter){
    
    float deposit;
    
    cout << "Deposit\n";
    cout << "Current Balance";
    cout << counter;
    cout << ": \n";
    cout << BankAccount::accountBalance;
    cout << "\n";
    cout << "Input The Amount: ";
    cin >> deposit;
    cout << "\n";

    BankAccount::accountBalance += deposit;
    
    cout << "\n" << endl;
}

void BankAccount::accWithdraw(int counter){
    
    float withdrawal;
    
    cout << "WITHDRAWAL\n";
    cout << "Account Balance";
    cout << counter;
    cout << ": \n";
    cout << BankAccount::accountBalance;
    cout << "\n";
    
    cout << "Input The Amount: ";
    cin >> withdrawal;
    cout << "\n";
    
    if(withdrawal > BankAccount::accountBalance){
        cout << "Not Enough Money in the account to withdraw ";
        cout << withdrawal;
        cout << "\n";
        cout << "Current Account Balance: ";
        cout << BankAccount::accountBalance;
        cout << "\n";
    }else{

        BankAccount::accountBalance -= withdrawal;
    }
    cout << "\n" << endl;
}

void BankAccount::accDetails(int counter){
    
    if ( BankAccount::accountNumber == 0)
    {
        cout << "Invalid Entry\n" << endl;
    }else{
        cout << "\n";
        cout << "Client Details ";
        cout << counter;
        cout << "\n";
        
        cout << "Client Name: ";
        cout << BankAccount::depositorName;
        cout << "\n";
        
        cout << "Type of Account: ";
        cout << BankAccount::accountType;
        cout << "\n";
        
        cout << "The Account Number: ";
        cout << BankAccount::accountNumber;
        cout << "\n";
        
        cout << "Balance: ";
        cout << BankAccount::accountBalance;
        cout << "\n";
        cout << "\n" << endl;
    }
}

int main()
{
    BankAccount acc[10];
    int select  = 1;
    int counter, clients;
    
    cout << "Number of Clients: ";
    cin >> clients;
    cout << "\n";
    if(clients > 10 ){
        cout << "Maximum number of clients is 10. \n" << endl;
    }else if(clients == 0 || clients < 0){
        cout << "Choose between 1 and 10 \n" << endl;
    }else{
        
        while (select != 0 )
        {
            cout << "Select Option\n";
            cout << "0. Input 0 to Exit\n";
            cout << "1. Create Account\n";
            cout << "2. Deposit\n";
            cout << "3. Withdraw\n";
            cout << "4. Account Details\n";
            
            cin >> select;
            switch(select)
            {
                case 0 :
                    cout<<"Program is exiting...";
                    break;
                case 1 :
                    for (counter = 1; counter <= clients; counter++){
                        acc[counter].registerAccount(counter);
                    }
                    break;
                case 2:
                    for (counter = 1; counter <= clients; ++counter)
                        acc[counter].accDeposit(counter);
                    break;
                case 3 :
                    for (counter = 1; counter <= clients; ++counter)
                        acc[counter].accWithdraw(counter);
                    break;
                case 4:
                    for (counter = 1; counter <= clients; ++counter)
                        acc[counter].accDetails(counter);
                    break;
                default: cout<<"Invalid Entry\n"<<endl;
            }
        }
    }
    return 0;
}
