//
//  main.c
//  Selection sort
//
//  Created by Mac on 10/18/18.
//  Copyright © 2018 PETER. All rights reserved.
//

#include <stdio.h>

int main() {
    int i,j,count,temp,array[20];
    printf("Enter number of elements:\n");
    scanf("%d",&count);
    printf("Enter %d elements:\n", count);
    
    for(i=0;i<count;i++)
        scanf("%d",&array[i]);
    
    for(i=0;i<count;i++){
        for(j=i+1;j<count;j++){
            if(array[i]>array[j]){
                temp=array[i];
                array[i]=array[j];
                array[j]=temp;
            }
        }
    }
    
    printf("Sorted elements:\n");
    for(i=0;i<count;i++)
        printf("%d\n",array[i]);
    return 0;
}
