//
//  main.c
//  Bubble sort
//
//  Created by Mac on 10/18/18.
//  Copyright © 2018 PETER. All rights reserved.
//

#include<stdio.h>
#include<ctype.h>

int main(){
    
    int count, temp, i, j, number[10];
    
    printf("Enter number of elements in the list: ");
    scanf("%d",&count);
    
    printf("Enter %d elements: ",count);
    
    for(i=0;i<count;i++)
        scanf("%d",&number[i]);
    
    
    for(i=count-2;i>=0;i--){
        for(j=0;j<=i;j++){
            if(number[j]>number[j+1]){
                temp=number[j];
                number[j]=number[j+1];
                number[j+1]=temp;
            }
        }
    }
    
    printf("Sorted elements: ");
    for(i=0;i<count;i++)
        printf(" %d",number[i]);
    
    return 0;
}
